//
//  DynamicArray.cpp
//  CommandLineTool
//
//  Created by Nathan Glyn Renney on 21/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#include "DynamicArray.h"

Array::Array()
{
    arraySize = 0;
    pArray = nullptr;
}

Array::~Array()
{
    delete [] pArray;
}

void Array::add(float itemValue)
{
    arraySize++;
    
    pArray = new float[arraySize];
    
    for (int i = 0; i < arraySize; i++)
    {
        if(pArray != nullptr)
            *pArray = itemValue;
        
    }
}

float Array::get(int index)
{
        return index;
}

int Array::size()
{
    return arraySize;
}