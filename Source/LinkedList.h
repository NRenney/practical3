//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Nathan Glyn Renney on 21/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__LinkedList__
#define __CommandLineTool__LinkedList__

#include <stdio.h>

class LinkedList
{
public:
    
    LinkedList();
    ~LinkedList();
    
    void add(float itemValue);
    /** adds new items to the end of the array */
    
    float get(int index);
    /** returns the value stored in the Node at the specified index */
    
    int size();
    /** returns the number of items currently in the linked list */
    
private:
    
    struct Node
    {
        float value;
        Node* next;
    };
    
    Node head;
};

#endif /* defined(__CommandLineTool__LinkedList__) */
