//
//  DynamicArray.h
//  CommandLineTool
//
//  Created by Nathan Glyn Renney on 21/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__DynamicArray__
#define __CommandLineTool__DynamicArray__

#include <stdio.h>

class Array
{
public:
    Array(); //Constuctor
    ~Array();//Destructor
    
    void add(float itemValue);
    /** adds new items to the end of the array */
    
    float get(int index);
     /** returns the item at the index */
    
    int size();
     /** returns the number of items currently in the array*/
    
private:
    
    int arraySize;
    float* pArray;

};

#endif /* defined(__CommandLineTool__DynamicArray__) */
